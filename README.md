Web Developer - Technical Test
==============================
Thank you for applying for a Web Developer position at Wholegrain Digital. We have prepared a short developer test for you to complete. This will help us assess if the role is a good match for your skills and interests. Our technical background is the usual LAMP environment, and our websites are mainly WordPress custom theme builds using Webpack, gulp, sass and es6.

Frontend Test
-------------
Write a HTML and CSS file that will output the included mockup. The mockup has been provided as a pdf as well as a sketch file. Additionally, assets that you can use to produce the mockup have been included. 

Aligning with our ethos, we are interested in hand crafted HTML and CSS that is accessible, performant and matches the design as closely as possible.

- There's no font file provided. Use a Google font in its place.
- The resulting page does not need to work in all browsers - just let us know what environment you built it in.
- We don't need to see the use of preprocessors or a build process - just write simple CSS.
- We're looking for hand-written CSS, not use of a front end framework.
- There is no mobile design provided so you will need to interpret the design and ensure that the page still works on mobile (320px) and fluidly in between mobile and desktop.

Backend Test
------------
We would like you to show your understanding of PHP & WordPress by building a plugin that registers a post type and taxonomy, and then imports content into the newly created post type and taxonomy.

See the markdown file in the backend-test folder for more information.

Notes
--------
- Please track your time and let us know how long the exercises took you. We suggest setting a 2 hour limit on each task and then noting down any further work that you did not get to.
- Please submit the work to a git repository and share the link with us or send us a zip file.
- Ask as many questions as you like
